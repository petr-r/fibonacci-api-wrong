﻿using System;
using System.Net;
using System.Web.Http;

namespace FibonacciApiWrong.Controllers
{
    public class FibonacciController : ApiController
    {
        [Route("fibonacci/{n}")]
        public IHttpActionResult Get(int n)
        {
            return Content(HttpStatusCode.OK, 42);
        }
    }
}